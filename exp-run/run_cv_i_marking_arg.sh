#!/usr/bin/env bash

IDX_FOLD=$1
PATH_DATA=$2
DATA_ID=$3
TRAIN_STEP=$4
rm ${PATH_DATA}/${DATA_ID}.data${IDX_FOLD}*.pt

python ../opennmt_py/preprocess.py \
-train_src ${PATH_DATA}/X_train_${IDX_FOLD}.tsv \
-train_tgt ${PATH_DATA}/Y_train_${IDX_FOLD}.tsv \
-valid_src ${PATH_DATA}/X_test_${IDX_FOLD}.tsv \
-valid_tgt ${PATH_DATA}/Y_test_${IDX_FOLD}.tsv \
-save_data ${PATH_DATA}/${DATA_ID}.data${IDX_FOLD} \
-src_words_min_frequency 0 \
-tgt_words_min_frequency 0 \

python ../opennmt_py/train.py \
-data ${PATH_DATA}/${DATA_ID}.data${IDX_FOLD} \
-save_model ${PATH_DATA}/${DATA_ID}-model${IDX_FOLD}  \
-encoder_type brnn \
-start_decay_steps 1000 \
-log_file ${PATH_DATA}/train_${IDX_FOLD}.log  \
-save_checkpoint_steps 400 \
-valid_steps 400 \
-train_steps ${TRAIN_STEP} \
-keep_checkpoint 1 \
-layers 2 \
-rnn_size 500 \
-gpu_ranks 0 \
-marking_condition .*\d$ \
-optim adagrad -learning_rate 0.2 -adagrad_accumulator_init 0.005 -max_grad_norm  2 \
-tensorboard -tensorboard_log_dir ${PATH_DATA}/tensorboard \
-dropout 0.5 \
-lambda_marking_mechanism 0.3 \

python ../opennmt_py/translate.py \
-model ${PATH_DATA}/${DATA_ID}-model${IDX_FOLD}_step_${TRAIN_STEP}.pt \
-src ${PATH_DATA}/X_test_${IDX_FOLD}.tsv \
-tgt ${PATH_DATA}/Y_test_${IDX_FOLD}.tsv \
-output ${PATH_DATA}/Y_pred_${IDX_FOLD}.tsv \
-replace_unk \
-report_time -verbose --log_file_level 0 \
-log_file ${PATH_DATA}/test_${IDX_FOLD}.log  \
-attn_debug

# evaluate acc
python ../src/eval_metrics.py \
--path ${PATH_DATA} \
--pred Y_pred_${IDX_FOLD}.tsv \
--target Y_test_${IDX_FOLD}.tsv \
> ${PATH_DATA}/result_${IDX_FOLD}.log

# print result
cat ${PATH_DATA}/result_${IDX_FOLD}.log
