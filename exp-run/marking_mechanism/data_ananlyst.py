from numpy import mean


class Word:
    rare_word_thres_hold = 3

    def __init__(self, w, frequency=0):
        self.w = w
        self.frequency = frequency

    def is_rare_word(self):
        return self.frequency < Word.rare_word_thres_hold

    def is_argument_word(self, ):
        return self.frequency < Word.rare_word_thres_hold

    def __eq__(self, other):
        return self.w == other.w

    def __hash__(self):
        return hash(self.w)

    def __str__(self):
        return "Word (val={}, frequency={})".format(self.w, self.frequency)


class DictAlphabet:
    def __init__(self):
        self.id2w = []
        self.w2id = {}

    def append(self, w: Word):
        if w in self.w2id:
            pass
        else:
            self.w2id[w] = len(self.id2w)
            self.id2w.append(w)
        self.id2w[self.w2id[w]].frequency += 1

    def intersect(self, other_dict):
        list_word = []
        for w in self.id2w:
            if w in other_dict.id2w:
                list_word.append(w)
        return list_word

    def print_stats(self, show_rare_word=False):
        print("Vocab size = {}".format(len(self.id2w)))

        count_rare_word = 0
        for w in self.w2id:
            if w.frequency <= Word.rare_word_thres_hold:
                count_rare_word += 1
                if show_rare_word:
                    print(w.w)
        print("Count rare word = {}".format(count_rare_word))


if __name__ == "__main__":

    files = [
        "raw/geo/data_processed.tsv",
        "raw/atis/data_processed.tsv",
        "raw/jobs/data_processed.tsv",
    ]
    for file_path in files:
        encoder_dict = DictAlphabet()
        decoder_dict = DictAlphabet()
        encoder_lengths = []
        decoder_lengths = []
        with open(file_path)  as f_data:
            for l in f_data.readlines():
                encoder, decoder = l.strip().split("\t")

                encoder_lengths.append(len(encoder.split(" ")))
                decoder_lengths.append(len(decoder.split(" ")))

                for w in encoder.split(" "):
                    encoder_dict.append(Word(w))
                for w in decoder.split(" "):
                    decoder_dict.append(Word(w))

        encoder_dict.print_stats(show_rare_word=False)
        decoder_dict.print_stats()
        print("Mean encoder length: {}".format(mean(encoder_lengths)))
        print("Mean decoder length: {}".format(mean(decoder_lengths)))

        list_word_intersect = encoder_dict.intersect(decoder_dict)
        list_word_intersect.sort(key=lambda x: (x.frequency), reverse=True)

        list_word_intersect_low_frequency = [w for w in list_word_intersect if w.frequency <= Word.rare_word_thres_hold]
        print(len(list_word_intersect_low_frequency), len(list_word_intersect),
              len(list_word_intersect_low_frequency) * 1.0 / len(list_word_intersect))
        print(file_path, "---")
        # for w in list_word_intersect:
        #     print(w)
