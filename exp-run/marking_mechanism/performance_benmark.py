import math


def compare_line(file_label_pred, file_label_gold, print_log=False):
    with open(file_label_gold) as f_label_gold:
        golds = f_label_gold.readlines()
    with open(file_label_pred) as f_label_pred:
        preds = f_label_pred.readlines()
    assert len(golds) == len(preds)
    count_true = 0
    for j, l in enumerate(golds):
        if l == preds[j]:
            count_true += 1
        else:
            if print_log is True:
                print("gold: ", l.strip())
                print("pred: ", preds[j].strip())
                print("---")
    return count_true, len(preds)


def mean(perfomances):
    return (sum(perfomances) + 0.0) / len(perfomances)


folders = ["raw/geo/", "raw/atis/", "raw/jobs/"]
for folder in folders:
    perfomances = []
    perfomances2 = []
    for i in range(10):
        file_label_pred = '{}/Y_pred_{}.tsv.src.label.pred.txt'.format(folder, i)
        file_label_gold = '{}/Y_pred_{}.tsv.src.label.gold.txt'.format(folder, i)
        file_logic_form_gold = '{}/Y_pred_{}.tsv.tgt.gold.txt'.format(folder, i)
        file_logic_form_pred = '{}/Y_pred_{}.tsv.tgt.pred.txt'.format(folder, i)

        count_true, count_total = compare_line(file_label_pred, file_label_gold)
        perfomances.append(count_true * 1.0 / count_total)
        print("- Data {} Label acc: {}".format(i, count_true * 1.0 / count_total))

        count_true, count_total = compare_line(file_logic_form_pred, file_logic_form_gold)
        perfomances2.append(count_true * 1.0 / count_total)
        print("- Data {} logic-form acc: {}".format(i, count_true * 1.0 / count_total))

    print("Accuracy Labeling Source Sentence  {}: {}".format(folder, mean(perfomances)))
    print("Accuracy Decoding Logical form  {}: {}".format(folder, mean(perfomances2)))
