#!/usr/bin/env bash

ROOT_DIR=$PWD

# ===================
# Some pre-processing
# prepare data artificial
cd $ROOT_DIR/data/artificial && python data_generator.py

# prepare data atis version (2) - warning: this step is time-consuming (> 30 min)
cd $ROOT_DIR/data/atis && python preprocessor.py

# prepare data geo version (2)
cd $ROOT_DIR/data/geo && python preprocessor.py

# prepare data jobs version (2)
cd $ROOT_DIR/data/jobs && python preprocessor.py

# ===================
# split 10 - fold each data-set
cd $ROOT_DIR/src && python split_data.py --path ../data/argument_atis --count_fold 10 --file_data_name data.tsv
cd $ROOT_DIR/src && python split_data.py --path ../data/argument_geo --count_fold 10 --file_data_name data.tsv
cd $ROOT_DIR/src && python split_data.py --path ../data/argument_jobs --count_fold 10 --file_data_name data.tsv
cd $ROOT_DIR/src && python split_data.py --path ../data/atis --count_fold 10 --file_data_name data_processed.tsv
cd $ROOT_DIR/src && python split_data.py --path ../data/geo --count_fold 10 --file_data_name data_processed.tsv
cd $ROOT_DIR/src && python split_data.py --path ../data/jobs --count_fold 10 --file_data_name data_processed.tsv
