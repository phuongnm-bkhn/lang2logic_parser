#!/usr/bin/env bash

IDX_FOLD=$1
PATH_DATA=$2
DATA_ID=$3
TRAIN_STEP=$4
LAMBDA_MARK=${5:=0.2}
BATCH_SIZE=${6:=32}
rm ${PATH_DATA}/${DATA_ID}.data${IDX_FOLD}*.pt

python ../opennmt_py/preprocess.py \
-train_src ${PATH_DATA}/X_train_${IDX_FOLD}.tsv \
-train_tgt ${PATH_DATA}/Y_train_${IDX_FOLD}.tsv \
-valid_src ${PATH_DATA}/X_test_${IDX_FOLD}.tsv \
-valid_tgt ${PATH_DATA}/Y_test_${IDX_FOLD}.tsv \
-save_data ${PATH_DATA}/${DATA_ID}.data${IDX_FOLD} \
-src_words_min_frequency 3 \
-tgt_words_min_frequency 3 \
#-dynamic_dict

python  ../opennmt_py/train.py \
-data ${PATH_DATA}/${DATA_ID}.data${IDX_FOLD} \
-save_model ${PATH_DATA}/${DATA_ID}-model${IDX_FOLD}  \
-layers 3 -rnn_size 512 -word_vec_size 512 -transformer_ff 2048 -heads 16  \
-encoder_type transformer-rnn -decoder_type transformer \
-train_steps ${TRAIN_STEP}  -max_generator_batches 2 -dropout 0.01 \
-batch_size ${BATCH_SIZE} -batch_type tokens -normalization tokens  -accum_count 2 \
-optim adam -adam_beta2 0.998 -decay_method noam -warmup_steps 100 -learning_rate 0.1 \
-max_grad_norm 0 -param_init 0  -param_init_glorot \
-label_smoothing 0.1 -valid_steps 2000 -save_checkpoint_steps 4000 -keep_checkpoint 1 \
-log_file ${PATH_DATA}/train_${IDX_FOLD}.log  \
-tensorboard -tensorboard_log_dir ${PATH_DATA}/tensorboard \
-world_size 1 -gpu_ranks 0 \
#-copy_attn -reuse_copy


python ../opennmt_py/translate.py \
-model ${PATH_DATA}/${DATA_ID}-model${IDX_FOLD}_step_${TRAIN_STEP}.pt \
-src ${PATH_DATA}/X_test_${IDX_FOLD}.tsv \
-tgt ${PATH_DATA}/Y_test_${IDX_FOLD}.tsv \
-output ${PATH_DATA}/Y_pred_${IDX_FOLD}.tsv \
-replace_unk \
-report_time -verbose --log_file_level 0 \
-log_file ${PATH_DATA}/test_${IDX_FOLD}.log  \
-attn_debug

# evaluate acc
python ../src/eval_metrics.py \
--path ${PATH_DATA} \
--pred Y_pred_${IDX_FOLD}.tsv \
--target Y_test_${IDX_FOLD}.tsv \
> ${PATH_DATA}/result_${IDX_FOLD}.log

me="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cp $DIR/$me ${PATH_DATA}/

# print result
cat ${PATH_DATA}/result_${IDX_FOLD}.log

python ${PATH_DATA}/../src/evaluate.py --path ${PATH_DATA}/ > ${PATH_DATA}/result_logic_${IDX_FOLD}.log
cat ${PATH_DATA}/result_logic_${IDX_FOLD}.log