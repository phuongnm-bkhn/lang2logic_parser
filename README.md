# Marking mechanism in Seq2seq 
This paper proposes a  novel approach  to solve this problem, namely Marking mechanism in Seq2seq. The main idea is to label special words which are rare-words in a sentence  by  the encoder (marking step) and the decoder represent the logical form based on those labels (transforming step). Our experiments demonstrate that this approach works effectively, achieved a competitive result with old methods on all 3 datasets Geo, Atis, Jobs and special outperformed on our Artificial dataset. 
![model](images/model.png)
## 1. Environments 

Install some libs for python
 
```commandline
pip install -r requirements.txt
```
## 2. Prepare data  

Run some pre-processing methods for raw data and split 10 fold. 

```commandline
bash exp-run/prepare_data.sh
```
> **Note**: Data on this repo is saved on github using [git lfs](https://help.github.com/en/articles/installing-git-large-file-storage). 
> If you want to download data, pls install git lfs and re-run `git lfs pull origin` to down load. 

## 3. Run experiments

 All experiments: 
 
 ```commandline
cd exp-run/

# data version 2
bash run_atis_cv_all.sh
bash run_geo_cv_all.sh
bash run_jobs_cv_all.sh


# data version 3
bash run_atis_arg_cv_all.sh
bash run_geo_arg_cv_all.sh
bash run_jobs_arg_cv_all.sh
 ```
 

> **Note**: The file `run_cv_i.sh` has default state using marking mechanism in Seq2seq. If you want 
to run copy mechanism or basline model, you should do some bellow steps:
>   1. checkout code branch **baseline_model**. ```cd opennmt_py && git checkout origin/baseline_model```
>   2. replace `run_cv_i.sh` by `run_cv_i_copy.sh` or `run_cv_i_baseline.sh`
>   3. To recovery experiments using marking mechanism, checkout code in folder`opennmt_py` on branch **marking_mechanism** 
>   and replace `run_cv_i.sh` by `run_cv_i_marking.sh`

## 4. Results

Results of experiment are saved in data folder. The file `result_i.log` is the result of fold i. Model also is saved 
in this folder such as `geo-model8_step_4000.pt` is name model of geo data of fold 8 step 4000. Especially, in the result 
of marking mechanism has some file name:
- `*.src.label.pred.txt` is file which saved the prediction labels of source sentence 
- `*.src.label.gold.txt` is file which saved the gold labels of source sentence
- `*.tgt.pred.txt` is file which saved the prediction of target sentence 
- `*.tgt.gold.txt` is file which saved the truth target sentence    
- `Y_pred_*.tsv` is file which saved the final prediction of testing data. 
- `train_*.log` is file which saved log of training process. 
- `test_*.log` is file which saved log of testing process (contain prediction each sample and attention matrix). 

