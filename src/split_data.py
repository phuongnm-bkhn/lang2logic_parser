import argparse
import logging
import re

import pandas as pd
from sklearn.model_selection import KFold

logger = logging.getLogger()


def norm_logic_form(logic_form):
    logic_form = re.sub(r"([\(\)])", r' \1 ', logic_form)
    logic_form = re.sub(r" {2,}", r' ', logic_form)
    return logic_form


def split_src_tgt_cv(path_base: str, data_raw: str, delimiter="\t", n_fold=10):
    path_file = f"{path_base}/{data_raw}"
    data = pd.read_csv(path_file, delimiter=delimiter, header=None)

    kf = KFold(n_splits=n_fold, shuffle=True, random_state=100)
    idx_fold = 0
    for train_index, test_index in kf.split(data):
        x_train, x_test = data[0][train_index], data[0][test_index]
        y_train, y_test = data[1][train_index], data[1][test_index]

        x_train.to_csv(f"{path_base}/X_train_{idx_fold}.tsv", index=False, sep="|")
        y_train.to_csv(f"{path_base}/Y_train_{idx_fold}.tsv", index=False, sep="|")
        x_test.to_csv(f"{path_base}/X_test_{idx_fold}.tsv", index=False, sep="|")
        y_test.to_csv(f"{path_base}/Y_test_{idx_fold}.tsv", index=False, sep="|")

        idx_fold += 1


def split_src_tgt(path_base: str, data_raw: str, type_data: str, delimiter="\t"):
    path_file = f"{path_base}/{data_raw}"
    data = pd.read_csv(path_file, delimiter=delimiter, header=None)

    data[0].to_csv(f"{path_base}/src-{type_data}.txt", index=False, header=False, sep="|")
    data[1].to_csv(f"{path_base}/tgt-{type_data}.txt", index=False, header=False, sep="|")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', default='../data/job_raw_arg',
                        required=True,
                        help="""Folder save  data.""")
    # setting for cv kfold
    parser.add_argument('--method', default='kfold_cv',
                        required=False,
                        help="""Init method kfold cv or hold_out""")
    parser.add_argument('--count_fold', default=10, type=int,
                        required=False,
                        help="""Count number of fold to split""")
    parser.add_argument('--file_data_name', default='data_processed.tsv',
                        required=False,
                        help="""Data file name to split train test""")
    # setting for holdout
    parser.add_argument('--type_data', default='train',
                        required=False,
                        help="""Type data: train/test/dev""")

    opt, unknown = parser.parse_known_args()
    if opt.method == "kfold_cv":
        split_src_tgt_cv(opt.path, opt.file_data_name, delimiter="\t", n_fold=opt.count_fold)
    elif opt.method == "hold_out":
        split_src_tgt(opt.path, opt.file_data_name, opt.type_data, delimiter="\t")
