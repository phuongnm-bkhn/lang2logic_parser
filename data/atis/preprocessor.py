import re


class E:
    def __init__(self, natural_text_, abbr_text_):
        self.natural_text = natural_text_.strip()
        self.abbr_text = abbr_text_.strip().replace(":", ":_")

    def __str__(self):
        return "E({}, {})".format(self.natural_text, self.abbr_text)


if __name__ == "__main__":
    print("Pre-processing Atis data.")
    with open("atis-db/lexicon.txt", "rt", encoding="utf-8") as f_lexical:
        lines = f_lexical.readlines()
        lexicons = []
        for l in lines:
            natural_text = l.split(":-")[0]
            abbr_text = l.split(" : ")[-1]
            lexicons.append(E(natural_text, abbr_text))
        lexicons.sort(key=lambda x: (len(x.natural_text)), reverse=True)

    with open("atis.tsv", "rt", encoding="utf-8") as f_data:
        new_lines = []
        old_lines = f_data.readlines()
        count_line = len(old_lines)
        for i, l in enumerate(old_lines):
            for lexicon in lexicons:
                # try:
                    l = re.sub("( |^|\n){}( |$|\n|\t)".format(lexicon.natural_text), "\\1 {} \\2".format(lexicon.abbr_text),
                           l)
                    l = re.sub(" {2,}", " ", l.replace(" \t", "\t"))
                # except:
                #     print(lexicon)
                #     exit(0)
            new_lines.append(l)
            print("Processed {}/{} line.".format(i, count_line))
        print("".join(new_lines))
        with open("data_processed.tsv", "wt", encoding="utf8") as fwrite:
            fwrite.write("".join(new_lines))

