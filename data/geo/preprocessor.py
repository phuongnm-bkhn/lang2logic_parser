import re


def norm_enc(enc_str: str):
    abbr_vocab = {
        "us": "usa",
        "united states": "usa",
        "america": "usa",
    }
    for k, v in abbr_vocab.items():
        enc_str = re.sub("( |^){}( |$)".format(k), '\\1{}\\2'.format(v), enc_str)
    return enc_str


def norm_dec(dec_str: str):
    return dec_str


if __name__ == "__main__":
    print("Pre-processing geos data.")
    new_lines = []
    with open("geo880.tsv", "rt", encoding="utf-8") as f_data:
        for l in f_data.readlines():
            enc, dec = l.strip().split("\t")
            enc = norm_enc(enc_str=enc)
            dec = norm_dec(dec_str=dec)
            new_lines.append("{}\t{}".format(enc, dec))
    with open("data_processed.tsv", "wt", encoding="utf-8") as f_write:
        f_write.write("\n".join(new_lines))
