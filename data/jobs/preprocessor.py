import re


def norm_enc(enc_str: str):
    return re.sub(r' {2,}', ' ', " ".join(enc_str.lower().replace("'", " ").split(","))).strip()


def norm_dec(dec_str: str, remove_coma=False):
    dec_str = dec_str.strip()

    result = set()
    var_names = re.findall(r'_\d+', dec_str)
    unique_varnames = []
    for x in var_names:
        if x not in unique_varnames:
            unique_varnames.append(x)
    var_names = unique_varnames
    alphabet = list('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
    for i, var_name in enumerate(var_names):
        dec_str = re.sub(r'{}([^\d])'.format(var_name), r'{}\1'.format(alphabet[i]), dec_str)

    # if re.search(r'\'[^\']\'', dec_str) is not None:
    dec_str = re.sub(r'\'([^\']*)\'', lambda pattern: "' " + pattern.group(1).lower() +" '", dec_str)
    dec_tokens = re.findall(r'(\\.|[A-Z]\d*|[,\(\)\']|\d+|[a-z_\+]*)', dec_str)
    dec_tokens = [x for x in dec_tokens if len(x) > 0]
    dec_str = " ".join(dec_tokens)
    if remove_coma:
        dec_str = dec_str.replace(" , ", " ")

    dec_str = re.sub(r' {2, }', " ", dec_str.strip())
    return dec_str


if __name__ == "__main__":
    print("Pre-processing jobs data.")
    new_lines = []
    with open("data.tsv", "rt", encoding="utf-8") as f_data:
        for l in f_data.readlines():
            enc, dec = l.split("\t")
            enc = norm_enc(enc_str=enc)
            dec = norm_dec(dec_str=dec, remove_coma=True)
            new_lines.append("{}\t{}".format(enc, dec))
    with open("data_processed.tsv", "wt", encoding="utf-8") as f_write:
        f_write.write("\n".join(new_lines))

