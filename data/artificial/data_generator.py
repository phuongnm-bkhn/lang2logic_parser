
import random

city_ids = [" ".join(["loc_id_{}{}".format(i, j) for j in range(random.choice([1, 2, 3]))]) for i in range(1000)]
city_test_ids = [" ".join(["loc_id_{}{}".format(i, j) for j in range(random.choice([1, 2, 3, 4, 5]))]) for i in range(1000)]

sentence_patern = []
logical_patern = []
with open("data.tsv", encoding="utf8") as pattern_file:
    for l in pattern_file.readlines():
        sentence, logic = l.strip().split("\t")
        sentence_patern.append(sentence)
        logical_patern.append(logic)

data_pair = []
lines_X = []
lines_Y = []
for i in range(len(sentence_patern)):
    cur_sentence_term = sentence_patern[i]
    cur_logical_term = logical_patern[i]
    for j in range(8):
        loc_id = random.choice(city_ids)
        lines_X.append(cur_sentence_term.format(loc_id))
        lines_Y.append(cur_logical_term.format(loc_id))
with open("X_train_0.tsv", "wt", encoding="utf8") as f_data:
    f_data.write("\n".join(lines_X))
with open("Y_train_0.tsv", "wt", encoding="utf8") as f_data:
    f_data.write("\n".join(lines_Y))

lines_X = []
lines_Y = []
for i in range(len(sentence_patern)):
    cur_sentence_term = sentence_patern[i]
    cur_logical_term = logical_patern[i]
    for j in range(2):
        loc_id = random.choice(city_test_ids)
        lines_X.append(cur_sentence_term.format(loc_id))
        lines_Y.append(cur_logical_term.format(loc_id))
with open("X_test_0.tsv", "wt", encoding="utf8") as f_data:
    f_data.write("\n".join(lines_X))
with open("Y_test_0.tsv", "wt", encoding="utf8") as f_data:
    f_data.write("\n".join(lines_Y))

